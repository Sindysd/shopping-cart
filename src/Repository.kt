interface Repository {
    suspend fun addItem(item: Item): Int
    suspend fun getItem(id: String): Item?
    suspend fun getItems(): List<Item>
    suspend fun getCart(id: String): Cart?
    suspend fun getCarts(): List<Cart>
    suspend fun addCart(cart: Cart): AddCartResponse
    suspend fun checkout(id: String): Int
    suspend fun setCartToExpire(id: String): Int
}