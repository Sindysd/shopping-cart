package channel

import org.slf4j.Logger

sealed class CartAction : Action {
    /**
     * Attempt to confirm/checkout the given [cartID]. The cart is not deleted after checkout
     */
    data class Checkout(val cartID: String, val cb: (Int) -> Unit) : CartAction()

    /**
     * Attempt to timeout a cart. The cart is not deleted after timeout but the items must be returned
     */
    data class Timeout(val cartID: String, val cb: (Int) -> Unit) : CartAction()

    /**
     * Attempt to clear items (return all items) from the given [cartID] but do not delete the cart itself
     */
    data class ClearCartItems(val cartID: String) : CartAction()
}

class CartChannelExecutor(val logger: Logger) : ChannelExecutor<CartAction>(logger) {

    private lateinit var checkout: (CartAction.Checkout) -> Unit
    private lateinit var timeout: (CartAction.Timeout) -> Unit
    private lateinit var clearCartItems: (CartAction.ClearCartItems) -> Unit

    fun initMethod(
        checkout: (CartAction.Checkout) -> Unit,
        timeout: (CartAction.Timeout) -> Unit,
        clearCartItems: (CartAction.ClearCartItems) -> Unit
    ) {
        this.checkout = checkout
        this.timeout = timeout
        this.clearCartItems = clearCartItems
    }
    override fun process(action: CartAction) {
        when (action) {
            is CartAction.Checkout -> checkout(action)
            is CartAction.Timeout -> timeout(action)
            is CartAction.ClearCartItems -> clearCartItems(action)
        }
    }

}