package channel

import kotlinx.coroutines.*
import kotlinx.coroutines.channels.Channel
import org.slf4j.Logger
import java.lang.Exception
import kotlin.coroutines.coroutineContext

interface Action

abstract class  ChannelExecutor<T: Action>(
    private val logger: Logger
) {
    protected val channel = Channel<T>()
    suspend fun start() = coroutineScope {
        launch {
            logger.debug("channel is running in $coroutineContext")
            /**
             * note: this looks like a normal loop. The inside is a suspending .hasNext() iterator. Check [CancellableContinuation]
             */
            for (action in channel) {
                /**
                 * exception and cancellation exception must be handled, so the channel does not crash and stop
                 * a trick from https://jivimberg.io/blog/2019/02/23/sqs-consumer-using-kotlin-coroutines/
                 */
                try {
                    logger.debug("channel get action $action")
                    process(action)
                } catch (cancellation: CancellationException) {
                    logger.warn("cancellation inside: $cancellation")
                    cancellation.printStackTrace()
                    break
                } catch (exception: Exception) {
                    logger.warn("exception inside: $exception")
                    exception.printStackTrace()
                }
            }

        }.invokeOnCompletion {
            //we should close the channel or else it'll be suspended indefinitely
            //https://www.techyourchance.com/kotlin-coroutines-in-complex-scenarios/
            logger.debug("channel closed (before): ${channel.isClosedForSend} ${channel.isClosedForReceive}")
            channel.close()
            logger.debug("channel closed (after): ${channel.isClosedForSend} ${channel.isClosedForReceive}")
        }
    }

    abstract fun process (action: T)

    suspend fun enqueue(action: T) = coroutineScope {
        launch {
            /**
             * note: since the channel is rendezvous (unbuffered), this will suspend until channel.receive() is called
             * more: https://kotlinlang.org/docs/reference/coroutines/channels.html#buffered-channels]
             */
            logger.debug("sending $action to channel, with context: $coroutineContext")
            channel.send(action)
        }
    }
}