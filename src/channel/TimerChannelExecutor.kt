package channel

import kotlinx.coroutines.*
import org.slf4j.Logger

enum class TimerStopResult {
    /**
     * timer was successfully stopped and removed
     */
    CANCEL_SUCCESS,

    /**
     * existing timer was found but it was completed just before cancellation, but was yet removed when the stop action is executed
     */
    JOB_HAS_COMPLETED,

    /**
     * no existing timer found. What happened can be either: 1) the timer was never exists; 2) the timer was stop/completed and removed just before this action
     */
    NO_TIMER_FOUND
}

sealed class TimerAction : Action {
    /**
     * this throws error if the timerId is duplicated. To reset, use [Reset]
     * start a timer. [timerId] must be unique
     */
    data class Set(val timerId: String, val duration: Long, val onTimeout:() -> Unit) : TimerAction()

    /**
     * reset current timer with new duration and new onTimeout callback.
     * [resetResultCallback] passes True if reset successful. False if reset was unsuccessful.
     * failure in reset might indicates either: 1) the timerId does not exists; or 2) the timer has just completed
     */
    data class Reset(val timerId: String, val newDuration: Long, val onTimeout: () -> Unit, val resetResultCallback: (Boolean) -> Unit) : TimerAction()

    /**
     * stop and remove timerId entry.
     * [callback] passes the stopping result as [TimerStopResult]
     */
    data class Stop(val timerId: String, val callback: ((TimerStopResult) -> Unit)? = null) : TimerAction()

    data class Test(val timerId: String) : TimerAction()
}

class TimerChannelExecutor(val logger: Logger) : ChannelExecutor<TimerAction>(logger) {
    private val jobs = hashMapOf<String, Job>()

    override fun process(action: TimerAction) {
        logger.info ("processing $action")
        when (action) {
            is TimerAction.Set -> set(action)
            is TimerAction.Reset -> reset(action)
            is TimerAction.Stop -> stop(action)
            is TimerAction.Test -> test(action)
        }
    }

    private fun set(action: TimerAction.Set) {
        //TODO racing condition with the delay... auto reset or not? let's see stop action...
        if (jobs.containsKey(action.timerId)) {
            logger.error("job contents: $jobs")
            error("duplicate timerId: ${action.timerId}")
        }

        val job =
            CoroutineScope(Dispatchers.Default).launch(start = CoroutineStart.LAZY) {
                logger.info ("start timer: ${action.timerId}")
                delay(action.duration)
                logger.info ("timer: ${action.timerId} wakes up")
            }

        job.invokeOnCompletion {
            logger.info ("removing timer: ${action.timerId}")
            jobs.remove(action.timerId)

            if (it is CancellationException) {
                //TODO provide callback if caller need to react differently when the action is successful or cancelled
                logger.info("timer ${action.timerId} was cancelled")
            } else {
                logger.info ("timer ${action.timerId} executing action")
                action.onTimeout()
            }
        }

        jobs.put(action.timerId, job)
        job.start()
        logger.info("contents of jobs: $jobs")
    }

    private fun reset (action: TimerAction.Reset) {
        val currentJob = jobs.get(action.timerId)
        if (currentJob == null) {
            logger.error("trying to reset an nonexistent timer: ${action.timerId}. The timer either was never exists or has just completed")
            action.resetResultCallback(false) //TODO a more descriptive error type which caller can inspect
        } else {
            val stopAction = TimerAction.Stop(action.timerId) {
                when (it) {
                    TimerStopResult.NO_TIMER_FOUND, TimerStopResult.JOB_HAS_COMPLETED -> {
                        logger.error("the timer ${action.timerId} has just completed, can't reset")
                        action.resetResultCallback(false)
                    }
                    TimerStopResult.CANCEL_SUCCESS -> {
                        val setAction = TimerAction.Set(action.timerId, action.newDuration, action.onTimeout)
                        CoroutineScope(Dispatchers.Default).launch {
                            enqueue(setAction)
                            action.resetResultCallback(true)
                        }
                    }
                }
            }

            CoroutineScope(Dispatchers.Default).launch {
                enqueue(stopAction)
            }

        }
    }

    private fun stop(action: TimerAction.Stop) {
        val job = jobs.get(action.timerId)
            ?: run {
                logger.info("timerId ${action.timerId} does not exists. it is either never exists or have completed")
                action.callback?.invoke(TimerStopResult.NO_TIMER_FOUND)
                return
            }

        /**
         * a check to examine racing condition between job completion and cancellation.
         * checking the status of the job works for simple cases, channel/serialization might be needed for a complex case
         * https://github.com/Kotlin/kotlinx.coroutines/issues/64#issuecomment-311023855
         */

        if (job.isCompleted) {
            if (job.isCancelled) logger.info ("timerId ${action.timerId} has been previously cancelled. no of ")
            else logger.info ("timerId ${action.timerId} has completed, no op from $action")
            action.callback?.invoke(TimerStopResult.JOB_HAS_COMPLETED)
        } else {
            logger.info ("cancel a running job")
            //TODO actually what happens when we cancel a completed job? is exception thrown
            job.cancel("$action was issued")
            action.callback?.invoke(TimerStopResult.CANCEL_SUCCESS)
        }

        jobs.remove(action.timerId)
    }

    private fun test(action: TimerAction.Test) {
        val timerId = "1"
        val duration = 1000L

        /*//following tests racing condition between timer completes and cancelled by external means
        logger.info ("starting test : $action")
        CoroutineScope(Dispatchers.Default).launch {
            logger.info ("sending set")
            enqueue(TimerAction.Set( timerId, duration) { logger.info ("timer 1 completed")})
            delay(duration-1)
            logger.info ("delay..")
            logger.info ("delay wakeup, sending stop")
            enqueue(TimerAction.Stop(timerId))
        }*/

        /*//following tests racing condition in reset behaviour
        CoroutineScope(Dispatchers.Default).launch {
            logger.info("sending set")
            enqueue (TimerAction.Set(timerId,duration) {})
            delay(duration-2)
            logger.info("sending reset")
            enqueue (
                TimerAction.Reset(
                    timerId,
                    duration,
                    onTimeout = { } ,
                    resetResultCallback = { success -> logger.info("success reset? $success")}
                )
            )
        }*/

        /*CoroutineScope(Dispatchers.Default).launch {
            logger.info ("sending set")
            enqueue(TimerAction.Set( timerId, duration) {
                error ("forced error")}
            )
        }*/
    }

}