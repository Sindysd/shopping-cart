package channel

import kotlinx.coroutines.channels.SendChannel
import org.slf4j.Logger

/**
 * if [success] is true, the item stock has been deducted. If not, the item does not have enough stock
 */
data class ReserveItemResult(val itemID: String, val success: Boolean)

sealed class ItemAction : Action {
    /**
     * attempt to reserve (stock-[count]) a given [itemID]. Pass the result to [resultChannel]
     */
    data class Reserve(val itemID: String, val count: Int, val resultChannel: SendChannel<ReserveItemResult>): ItemAction()

    /**
     * attempt to return (stock+[count]) a given [itemID]
     */
    data class Return(val itemID: String, val count: Int) : ItemAction()
}

class ItemChannelExecutor(
    val logger: Logger
) : ChannelExecutor<ItemAction>(logger) {
    private lateinit var reserve: (ItemAction.Reserve) -> Unit
    private lateinit var returnItem: (ItemAction.Return) -> Unit
    fun initMethod(
        //TODO we can also pass an interface
        reserve: (ItemAction.Reserve) -> Unit,
        returnItem: (ItemAction.Return) -> Unit
    ) {
        this.reserve = reserve
        this.returnItem = returnItem
    }

    override fun process(action: ItemAction) {
        when(action) {
            is ItemAction.Reserve -> reserve(action)
            is ItemAction.Return -> returnItem(action)
        }
    }
}