import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonDeserializer
import com.fasterxml.jackson.databind.JsonSerializer
import com.fasterxml.jackson.databind.SerializerProvider
import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import com.fasterxml.jackson.databind.annotation.JsonSerialize
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

enum class CartStatus(val value: String){
    NEW("new"),
    COMPLETED("completed"),
    TIMEOUT("timeout"),
    UNKNOWN("unknown");

    companion object {
        private val valueMap = values().associateBy { it.value }
        fun fromValue(value: String) = valueMap[value]?: UNKNOWN
    }
}

val dateFormatter = DateTimeFormatter.ISO_LOCAL_DATE_TIME

interface LocalDateTimeParser {
    fun createLocalDateTime(string: String): LocalDateTime? =
        kotlin.runCatching { LocalDateTime.parse(string, dateFormatter) }.getOrNull()

    fun createString(localDateTime: LocalDateTime) =
        kotlin.runCatching { localDateTime.format(dateFormatter) }.getOrNull()
}

data class Item(
    val id: String,
    val name: String,
    @field:JsonProperty("available_stock") val availableStock: Int,

    @field:JsonDeserialize(using = LocalDateTimeDeserializer::class)
    @field:JsonSerialize(using = LocalDateTimeSerializer::class)
    @field:JsonProperty("created_at") val createdAt: LocalDateTime? = null,

    @field:JsonDeserialize(using = LocalDateTimeDeserializer::class)
    @field:JsonSerialize(using = LocalDateTimeSerializer::class)
    @field:JsonProperty("updated_at") val updatedAt: LocalDateTime? = null

) {
    @field:JsonIgnore val createdAtString = createdAt?.let { createString(it) }
    @field:JsonIgnore val updatedAtString = updatedAt?.let { createString(it) }

    companion object : LocalDateTimeParser
}

data class CartItem(
    @field:JsonProperty("item_id") val itemId: String,
    val count: Int,

    @field:JsonDeserialize(using = LocalDateTimeDeserializer::class)
    @field:JsonSerialize(using = LocalDateTimeSerializer::class)
    @field:JsonProperty("added_at") val addedAt: LocalDateTime? = null
) {
    @field:JsonIgnore val addedAtString = addedAt?.let { createString(it) }

    companion object : LocalDateTimeParser
}

data class Cart(
    val id: String,
    val items: List<CartItem>,

    @field:JsonDeserialize(using = LocalDateTimeDeserializer::class)
    @field:JsonSerialize(using = LocalDateTimeSerializer::class)
    @field:JsonProperty("created_at") val createdAt: LocalDateTime? = null,
    val status: CartStatus? = CartStatus.NEW
) {
    @field:JsonIgnore val createdAtString = createdAt?.let { createString(it) }
    companion object : LocalDateTimeParser
}

data class AddCartResponse(
    val addedItem: List<CartItem>,
    val failedItem: List<CartItem>
)

private val localDateTimeParseImpl = object : LocalDateTimeParser {

}
class LocalDateTimeSerializer : JsonSerializer<LocalDateTime>(){
    override fun serialize(value: LocalDateTime?, gen: JsonGenerator, sp: SerializerProvider) {
        value?.let {
            gen.writeString(localDateTimeParseImpl.createString(it))
        }
    }
}

class LocalDateTimeDeserializer : JsonDeserializer<LocalDateTime>() {
    override fun deserialize(parser: JsonParser, ctx: DeserializationContext?): LocalDateTime {
        val value = parser.valueAsString
        return localDateTimeParseImpl.createLocalDateTime(value)?: error("invalid localdatetime: $value")
    }

}