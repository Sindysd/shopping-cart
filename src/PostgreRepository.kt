import com.icehouse.learning.CART_TIMEOUT_MILLIS
import kotlinx.coroutines.*
import org.jdbi.v3.core.Handle
import org.jdbi.v3.core.Jdbi
import org.jdbi.v3.core.kotlin.KotlinPlugin
import org.jdbi.v3.core.statement.Query
import org.jdbi.v3.sqlobject.kotlin.KotlinSqlObjectPlugin
import org.slf4j.Logger
import java.sql.Timestamp
import java.time.LocalDateTime
import java.time.ZoneOffset
import javax.sql.DataSource

class PostgreRepository(
    dataSource: DataSource,
    val log: Logger
) : Repository {

    private val jdbi: Jdbi = Jdbi.create(dataSource).apply {
        installPlugin(KotlinPlugin())
        installPlugin(KotlinSqlObjectPlugin())
    }

    override suspend fun addItem(item: Item): Int {
        return executeInIO { it.inTransaction<Int, Exception> { handle ->
            val resultCount = handle.createUpdate(
                "INSERT INTO item (item_id, item_name, stock, initial_stock, created_at) VALUES " +
                        "(:id, :name, :stock, :initial_stock, :created_at);")
                .bind("id", item.id)
                .bind("name", item.name)
                .bind("stock", item.availableStock)
                .bind("initial_stock", item.availableStock)
                .bind("created_at", LocalDateTime.now(ZoneOffset.UTC))
                .execute()
            handle.commit()
            resultCount
        } }.also { log.info("inserted $it item to db")}
    }

    override suspend fun getItem(id: String): Item? {
        return executeInIO { handle ->
            handle.select("SElECT item_id, item_name, stock, created_at, updated_at FROM item WHERE item_id = ?", id)
                .mapToMap()
                .map {
                    log.info("data in db $it")
                    it.toItem() }
                .firstOrNull()
        }
    }

    override suspend fun getItems(): List<Item> {
        return executeInIO { handle ->
            handle.select("SELECT item_id, item_name, stock, created_at FROM item")
                .mapToMap()
                .map { it.toItem() }
                .toList()
        }
    }

    private fun Map<String, Any>.toItem() = Item(
        this["item_id"].toString(),
        this["item_name"].toString(),
        this["stock"] as Int,
        (this["created_at"] as Timestamp).toLocalDateTime(),
        (this["updated_at"] as Timestamp?)?.toLocalDateTime()
    )

    override suspend fun getCart(id: String): Cart? {
        val dbData = executeInIO { handle ->
            handle.select(
                "SELECT c.cart_id, c.status, c.created_at, ci.item_id, ci.item_count, ci.added_at " +
                        "FROM cart c JOIN cart_item ci ON c.cart_id = ci.cart_id WHERE c.cart_id = ?", id)
                .toListOfMap()
        }

        val cartItems = dbData.map {
            it.toCartItem()
        }

        val cart = dbData.first().let {
            Cart(
                id = it["cart_id"]?.toString()!!,
                items = cartItems,
                createdAt = (it["created_at"] as Timestamp).toLocalDateTime(),
                status = CartStatus.fromValue(it["status"].toString())
            )
        }

        return cart
    }

    override suspend fun getCarts(): List<Cart> {
        val carts = mutableMapOf<Cart, MutableList<CartItem>>()
        executeInIO { handle ->
            handle.select(
                "SELECT c.cart_id, c.status, c.created_at, ci.item_id, ci.item_count, ci.added_at " +
                        "FROM cart c JOIN cart_item ci ON c.cart_id = ci.cart_id")
                .toListOfMap()
        }.map {
            val cartId = it["cart_id"]?.toString()!!
            val cartItem = it.toCartItem()

            carts.keys.firstOrNull { it.id == cartId }?.let { existingCart -> carts[existingCart]!!.add(cartItem) }
                ?: run {
                    val newCart = Cart(
                        id = cartId,
                        items = emptyList(),
                        createdAt = (it["created_at"] as Timestamp).toLocalDateTime(),
                        status = CartStatus.fromValue(it["status"].toString())
                    )

                    carts.put(newCart, mutableListOf(cartItem))

                }
        }

        return carts.map { it.key.copy(items = it.value) }
    }

    private fun Query.toListOfMap(): List<Map<String, Any>> {
        val result: MutableList<Map<String, Any>> = mutableListOf()

        this
            .mapToMap()
            .asIterable()
            .also { result.addAll(it) }

        return result
    }


    private fun Map<String, Any>.toCartItem() = CartItem(
        this["item_id"].toString(),
        this["item_count"] as Int,
        (this["added_at"] as Timestamp).toLocalDateTime()
    )

    /**
     * several strategies in the database:
     * https://www.2ndquadrant.com/en/blog/postgresql-anti-patterns-read-modify-write-cycles/
     */
    override suspend fun addCart(cart: Cart): AddCartResponse {
        val addedItems = mutableListOf<CartItem>()
        val failedItems = mutableListOf<CartItem>()
        var firstItem = true

        cart.items.forEach { cartItem ->
            val item = getItem(cartItem.itemId)?: run {
                print("item id: ${cartItem.itemId} not found")
                failedItems.add(cartItem)
                return@forEach
            }

            executeInIO { it.inTransaction<Unit, Exception> { handle ->
                val currentStock = handle.select(
                    "SELECT stock FROM item WHERE item_id = ? AND stock >= ? FOR UPDATE",
                    cartItem.itemId,
                    cartItem.count
                ).mapTo(Int::class.java).firstOrNull()
                log.info("${Thread.currentThread().name} current item stock: $currentStock")

                if (currentStock != null) {
                    val actionTime = LocalDateTime.now(ZoneOffset.UTC)

                    handle.execute(
                        "UPDATE item SET stock = ?, updated_at = ? WHERE item_id = ?",
                        currentStock - cartItem.count,
                        actionTime,
                        item.id
                    )

                    if (firstItem) {
                        handle.execute(
                            "INSERT INTO cart (cart_id, status, created_at) VALUES (?,?,?);",
                            cart.id,
                            CartStatus.NEW.value,
                            actionTime
                        ).also { if (it != 1) error("") }

                        firstItem = false
                    }

                    handle.execute(
                        "INSERT INTO cart_item (cart_id, item_id, item_count, added_at) VALUES(?,?,?,?)",
                        cart.id,
                        cartItem.itemId,
                        cartItem.count,
                        actionTime
                    ).also { if (it != 1) error("")}

                    handle.commit() //TODO commit here makes when 2nd item fails, 1st item still inserted

                    addedItems.add(cartItem.copy(addedAt = actionTime))
                    log.info("successfully add $cartItem to cartId: ${cart.id}")
                } else {
                    failedItems.add(cartItem)
                    handle.rollback()
                    log.info("failed to add $cartItem to cartId: ${cart.id}")
                }
            } }
        }

        if (addedItems.isNotEmpty()) {
            //TODO store a reference to the timer, stop the timer when the cart gets checked-out/cancel. Maybe coroutine's stream?
            /**
             * TODO other approach in the timeout?
             *
             *  1) 1 timer per cart (current)
             *  when there are n carts, there are also n timers.
             *  downsides:
             *      - more carts = more timers
             *  upsides:
             *      - precise timing
             *
             *  2) a scheduled job to check each database entry that has passed a certain time
             *  downside:
             *      - approximate, not so precise, timing. worst case: a cart that is made 1 unit of time after the last job finishes, will wait 2 rounds
             *  upside:
             *      - 1 timer job for all carts
             *
             */
            CoroutineScope(Dispatchers.Default).launch {
                log.info("${Thread.currentThread().name} start expiration timer on cart ${cart.id}")
                delay(CART_TIMEOUT_MILLIS)
                log.info("${Thread.currentThread().name} delay wakeup. attempt to set cart ${cart.id} to expire...")
                setCartToExpire(cart.id)

                //TODO handle/propagate/log when: 1) sth is wrong with the expiration; 2) when the status has been changed to other than "NEW"
                log.info("${Thread.currentThread().name} stop expiration timer on cart ${cart.id}")
            }
        }
        return AddCartResponse(addedItems, failedItems)
    }

    override suspend fun checkout(id: String): Int {
        return updateCartStatus(id, CartStatus.COMPLETED, CartStatus.NEW)
    }

    private suspend fun updateCartStatus(id: String, newCartStatus: CartStatus, currentCartStatus: CartStatus): Int  {
        return executeInIO {it.inTransaction<Int, Exception> { handle ->
            val result = handle.createUpdate(
                "UPDATE cart SET status = :new_status WHERE cart_id = :cart_id AND status = :current_status"
            )
                .bind("new_status", newCartStatus.value)
                .bind("cart_id", id)
                .bind("current_status", currentCartStatus.value)
                .execute()

            handle.commit()
            result
        } }.also {
            when (it){
                0 -> log.info("cannot find cart id: $id with status: ${currentCartStatus.value}")
                1 -> log.info("successfully updated cart: $id to status ${newCartStatus.value}")
                else -> error("multiple cart ($it) updated")
            }
        }
    }

    override suspend fun setCartToExpire(id: String): Int {
        val expirationResult = updateCartStatus(id, CartStatus.TIMEOUT, CartStatus.NEW)
        if (expirationResult != 1) return expirationResult

        val cart = getCart(id)?: error("cart not found")

        cart.items.forEach { cartItem ->
            executeInIO {it.inTransaction<Unit, Exception> { handle ->
                val currentStock = handle.select(
                    "SELECT stock FROM item WHERE item_id = ? FOR UPDATE;",
                    cartItem.itemId
                ).mapTo(Int::class.java).first()

                handle.execute(
                    "UPDATE item SET stock = ? WHERE item_id = ?;",
                    currentStock + cartItem.count,
                    cartItem.itemId
                ).also { if (it != 1) error ("multiple items stock updated: currentStock:$currentStock, cartItem: $cartItem")}
                handle.commit()
            } }
        }

        return expirationResult
    }

    /**
     * interesting trick from https:// www.raywenderlich.com/7265034-ktor-rest-api-for-mobile#toc-anchor-006
     * this share threads pool with Dispatchers.DEFAULT, but different max pool size
     * https://stackoverflow.com/questions/59039991/difference-between-usage-of-dispatcher-io-and-default
     */
    suspend fun <T> executeInIO(block: (Handle) -> T): T {
        return withContext(Dispatchers.IO) {
            jdbi.withHandle<T, Exception>() {
                //TODO query logger by the jdbi
                    block(it)
            }
        }
    }
}