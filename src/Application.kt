package com.icehouse.learning

import Cart
import CartItem
import Item
import channel.ItemChannelExecutor
import RedisRepository
import channel.CartChannelExecutor
import channel.TimerAction
import channel.TimerChannelExecutor
import io.ktor.application.*
import io.ktor.response.*
import io.ktor.request.*
import io.ktor.routing.*
import io.ktor.http.*
import com.fasterxml.jackson.databind.*
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import io.ktor.client.HttpClient
import io.ktor.client.engine.cio.CIO
import io.ktor.client.features.HttpTimeout
import io.ktor.client.features.json.JacksonSerializer
import io.ktor.client.features.json.JsonFeature
import io.ktor.client.request.HttpRequestBuilder
import io.ktor.client.request.header
import io.ktor.client.request.post
import io.ktor.client.statement.HttpResponse
import io.ktor.content.TextContent
import io.ktor.jackson.*
import io.ktor.features.*
import io.ktor.server.engine.applicationEngineEnvironment
import io.ktor.server.engine.embeddedServer
import io.ktor.server.engine.stop
import io.ktor.server.netty.Netty
import kotlinx.coroutines.*
import org.slf4j.event.Level
import redis.clients.jedis.JedisPool
import redis.clients.jedis.JedisPoolConfig
import java.util.*
import java.util.concurrent.TimeUnit

fun main(args: Array<String>): Unit {
    /**
     * trick from https://dev.to/viniciusccarvalho/graceful-shutdown-of-ktor-applications-1h53
     * to give some time to the app to clean up resources upon being killed by SIGINT
     */

    val server = embeddedServer(Netty, port = 8080) {
        module()
    }.start(false)

    //TODO what does this actually do?
    Runtime.getRuntime().addShutdownHook ( Thread {
        server.stop(1, 5, TimeUnit.SECONDS)
    })
    Thread.currentThread().join()

}
const val CART_TIMEOUT_MILLIS: Long = 10000
@Suppress("unused") // Referenced in application.conf
@kotlin.jvm.JvmOverloads
fun Application.module(testing: Boolean = false) {
    //TODO this is getting messy... make changing repository an "install" feature?
    // val repository = PostgreRepository(buildHikariDataSource(), log)

    val itemChannel = ItemChannelExecutor(log).also {
        CoroutineScope(coroutineContext).launch { it.start() }
    }

    val cartChannel = CartChannelExecutor(log).also {
        CoroutineScope(coroutineContext).launch { it.start() }
    }

    val timerChannel = TimerChannelExecutor(log).also {
        CoroutineScope(coroutineContext).launch {
            it.start()
        }
    }

    //TODO try kotlin serialization
    val client = HttpClient(CIO) {
        install(JsonFeature) {
            serializer = JacksonSerializer()
        }

        install(HttpTimeout) {
            requestTimeoutMillis = 20000
            connectTimeoutMillis = 2000
        }
    }

    val jedisPool = JedisPool(JedisPoolConfig())

    environment.monitor.subscribe(ApplicationStarted) {
        log.info ("......started")
    }

    environment.monitor.subscribe(ApplicationStopped) {
        log.info(".......stopped")
        jedisPool.close()
        client.close()
    }

    val repository = RedisRepository(log, jedisPool, itemChannel, cartChannel, timerChannel)

    install(ContentNegotiation) {
        jackson {
            enable(SerializationFeature.INDENT_OUTPUT)
        }
    }

    install(CallId) {
        retrieve {
            it.request.header(HttpHeaders.XCorrelationId).takeIf { !it.isNullOrBlank() }
        }

        generate {
            "generated-${UUID.randomUUID()}"
        }
    }

    install(CallLogging) {
        level = Level.INFO

        callIdMdc("correlation-id")
    }

    routing {
        route("/item") {
            get("") {
                val items = repository.getItems()
                call.respond(HttpStatusCode.OK, items)
            }

            get("{id}") {
                val id = call.parameters["id"]!!
                val item = repository.getItem(id)
                item?.let {
                    call.respond(item)
                } ?: run {
                    call.response.status(HttpStatusCode.NotFound)
                }
            }

            post("") {
                val newItem = call.receive<Item>()
                log.info("newItem: ${newItem}")
                repository.addItem(newItem)
                log.info("insert finish")
                call.response.status(HttpStatusCode.Created)
            }

            get("/json/jackson") {
                call.respond(mapOf("hello" to "world"))
            }
        }

        route("/cart") {
            get("") {
                repository.getCarts()
                    .also { call.respond(it) }
            }

            get("/{id}") {
                val id = call.parameters["id"]!!
                repository.getCart(id)
                    ?.also { call.respond(it) }
                    ?: run  { call.response.status(HttpStatusCode.NotFound) }
            }

            post("") {
                val newCart = call.receive<Cart>()
                val response = repository.addCart(newCart)
                call.response.status(HttpStatusCode.Created)
                call.respond(response)
            }

            post("/{id}/checkout") {
                val id = call.parameters["id"]!!
                repository.checkout(id)
                    .also {
                        call.respond(it)
                    }
            }
        }

        route("/test") {
            val rootUrl = "http://0.0.0.0:8080"
            post("/timer") {
                timerChannel.enqueue(TimerAction.Test("test from app"))
                call.respond(HttpStatusCode.OK)
            }
            post(""){
                //TODO initiate one client, close the client when server dies (find the correct pipeline)
                val objectMapper = jacksonObjectMapper()
                val correlationId: HttpRequestBuilder.(String) -> Unit = { correlationId ->
                    header(HttpHeaders.XCorrelationId, correlationId)
                }

                val cart1 = Cart(
                    id = UUID.randomUUID().toString(),
                    items = listOf(
                        CartItem("1", 1),
                        CartItem("2", 1)
                    )
                )

                val cart2 = Cart(
                    id = UUID.randomUUID().toString(),
                    items = listOf(
                        CartItem("2", 1),
                        CartItem("3", 1)
                    )
                )

                val cart1Job = launch {
                    val cart1CorrelationId = cart1.id
                    println("posting cart 1:$cart1")
                    val cart1Response = client.post<HttpResponse>("$rootUrl/cart") {
                        body = TextContent(objectMapper.writeValueAsString(cart1), ContentType.Application.Json)
                        correlationId(cart1CorrelationId)
                    }
                    log.info("cart 1 response: $cart1Response")
                }

                val cart2Job = launch {
                    val cart2CorrelationId = cart2.id
                    println("posting cart 2:$cart2")
                    val cart2Response = client.post<HttpResponse>("$rootUrl/cart") {
                        body = TextContent(objectMapper.writeValueAsString(cart2), ContentType.Application.Json)
                        correlationId(cart2CorrelationId)
                    }
                    log.info("cart 2 response: $cart2Response")
                }

                delay(CART_TIMEOUT_MILLIS-10)
                val checkoutCart1Job = launch {
                    client.post("$rootUrl/cart/${cart1.id}/checkout") {
                        correlationId("checkout-${cart1.id}")
                    }
                }

                joinAll(cart1Job, cart2Job, checkoutCart1Job)

                call.response.status(HttpStatusCode.OK)
            }
        }
    }
}
