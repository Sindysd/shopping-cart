import com.zaxxer.hikari.HikariConfig
import com.zaxxer.hikari.HikariDataSource

//https://www.raywenderlich.com/7265034-ktor-rest-api-for-mobile#toc-anchor-006
//https://www.novatec-gmbh.de/en/blog/creating-a-rest-application-with-ktor-and-exposed/
//https://www.thebookofjoel.com/kotlin-ktor-exposed-postgres
fun buildHikariDataSource(): HikariDataSource {
    val config = HikariConfig("/hikari.properties")
    config.isAutoCommit = false
    return HikariDataSource(config)
}