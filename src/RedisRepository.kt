import channel.*
import com.icehouse.learning.CART_TIMEOUT_MILLIS
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.consumeAsFlow
import kotlinx.coroutines.flow.take
import org.slf4j.Logger
import redis.clients.jedis.Jedis
import redis.clients.jedis.JedisPool
import redis.clients.jedis.ScanParams
import java.time.LocalDateTime
import java.time.ZoneOffset
import kotlin.coroutines.coroutineContext

private val redisAcceptableRegex = Regex("[^:]+?")

private object ItemMapper {
    val KEY = "item:"
    val NAME = "name"
    val CREATED_AT = "created-at"
    val UPDATED_AT = "updated-at"
    val STOCK = "stock"

    fun createHashKey(itemID: String) = "$KEY$itemID"
    fun parseItemIDFromKey(key: String) = key.split(":").last()
    fun createObject(map: Map<String, String>, id: String): Item {
        val name = map.get(NAME)!!
        val createdAt = map.get(CREATED_AT)?.takeIf { it.isNotBlank() }?.let { Item.createLocalDateTime(it)}
        val updatedAt = map.get(UPDATED_AT)?.takeIf { it.isNotBlank() }?.let { Item.createLocalDateTime(it)}
        val availableStock = map.get(STOCK)?.toIntOrNull()?:0
        return Item(id, name, availableStock, createdAt, updatedAt)
    }

    fun createValueMap(item: Item): Map<String, String>{
        val map =  mutableMapOf(
            NAME to item.name,
            STOCK to item.availableStock.toString()
        )

        item.createdAtString?.also { map.put(CREATED_AT, it) }
        item.updatedAtString?.also { map.put(CREATED_AT, it) }

        return map
    }

    fun isValidString(string: String) = redisAcceptableRegex.matches(string)
    fun allValid(item: Item): Boolean = isValidString(item.id) && CartItemMapper.isValidString(item.id) && isValidString(item.name)
}

private object CartMapper {
    val KEY = "cart:"
    val CREATED_AT = "created-at"
    val STATUS = "status"

    fun createHashKey(cartID: String) = "$KEY$cartID"
    fun parseCartIDFromKey(key: String) = key.split(":").last()
    fun createObject(map: Map<String, String>, id: String): Cart {
        val createdAt = map.get(CREATED_AT)?.takeIf { it.isNotBlank() }?.let { Cart.createLocalDateTime(it) }
        val status = CartStatus.fromValue(map.get(STATUS)!!)
        return Cart(id, emptyList(), createdAt, status)
    }

    fun createValueMap(cart: Cart): Map<String, String> {
        val map = mutableMapOf(
            STATUS to (cart.status?.value?: CartStatus.UNKNOWN.value)
        )
        cart.createdAtString?.also { map.put (CREATED_AT, it) }
        return map
    }

    fun isValidString(string: String) = redisAcceptableRegex.matches(string)
    fun allValid(cart: Cart) = isValidString(cart.id)
}

//TODO inspect the returned value from redis and decide if an action is successful or not
private object CartItemMapper {
    val KEY = "cart-item:"
    val FLATTEN_DELIMITER = "#"

    val delimiterRegex = Regex("[^#]")
    fun createHashKey(cartId: String) = "$KEY$cartId"
    fun flatten(item:CartItem) = "${item.itemId}$FLATTEN_DELIMITER${item.count}$FLATTEN_DELIMITER${item.addedAtString?: ""}"
    fun createObject(flattened: String): CartItem {
        val data = flattened.split(FLATTEN_DELIMITER)
        val itemID = data[0]
        val count = data[1].toIntOrNull()?:0
        val addedAt = data.getOrNull(2)?.takeIf { it.isNotBlank() }?.let { CartItem.createLocalDateTime(it) }

        return CartItem(
            itemID,
            count,
            addedAt
        )
    }

    fun isValidString(string: String) = delimiterRegex.matches(string)
}

class RedisRepository(
    val logger: Logger,
    val jedisPool: JedisPool,
    val itemChannel: ItemChannelExecutor,
    val cartChannel: CartChannelExecutor,
    val timerChannel: TimerChannelExecutor
) : Repository {
    init {
        itemChannel.initMethod(
            reserve = ::reserveItem,
            returnItem = ::returnItem
        )

        cartChannel.initMethod(
            checkout = ::internalCheckout,
            timeout = ::internalTimeout,
            clearCartItems = ::clearCartItems
        )
    }

    override suspend fun addItem(item: Item): Int {
        if (!ItemMapper.allValid(item)) error ("$item has fails regex")
        val toSave = if (item.createdAt == null) item.copy(createdAt = LocalDateTime.now(ZoneOffset.UTC)) else item
        return withJedis { hset(ItemMapper.createHashKey(item.id), ItemMapper.createValueMap(toSave))}.toInt()
    }

    override suspend fun getItem(id: String): Item? {
        return withJedis { hgetAll(ItemMapper.createHashKey(id)) }
            .takeIf { it.isNotEmpty() }
            ?.let { ItemMapper.createObject(it, id) }
            .also { logger.debug("get item: $it") }
    }

    //TODO not so reliable but it works for small case
    override suspend fun getItems(): List<Item> {
        val itemKeys = withJedis { scan(
            ScanParams.SCAN_POINTER_START,
            ScanParams()
                .count(100)
                .match("${ItemMapper.KEY}*")
        )}.result

        //TODO redis pipelining to reduce call
        return itemKeys.map {
            val itemId = ItemMapper.parseItemIDFromKey(it)
            getItem(itemId)
        }.filterNotNull()
    }

    override suspend fun getCart(id: String): Cart? {
        val cartKey = CartMapper.createHashKey(id)
        val cart = withJedis { hgetAll(cartKey) }
            .takeIf { it.isNotEmpty() }
            ?.let { CartMapper.createObject(it, id) }
            .also { logger.debug("get cart: $it") }
            ?: return null

        val cartItems = withJedis { smembers(CartItemMapper.createHashKey(id)) }
            .map { flattened -> CartItemMapper.createObject(flattened) }

        return cart.copy(items = cartItems)
    }

    //TODO not so reliable but it works for small case
    override suspend fun getCarts(): List<Cart> {
        return withJedis { scan(
            ScanParams.SCAN_POINTER_START,
            ScanParams()
                .count(100)
                .match("${CartMapper.KEY}*")
        ) }
            .result
            .also { logger.debug(("cart ids from redis: $it")) }
            .map {  key ->
                //TODO redis pipeline to reduce call
                val cartID = CartMapper.parseCartIDFromKey(key)
                getCart(cartID)
            }
            .filterNotNull()
    }

    override suspend fun addCart(cart: Cart): AddCartResponse {
        if (!CartMapper.allValid(cart)) error ("$cart fails regex")
        val currentCart = getCart(cart.id)
        if (currentCart != null){
            logger.info("duplicate cart")
            val stopResult = suspendCancellableCoroutine<TimerStopResult> { continuation ->
                val stopAction = TimerAction.Stop(cart.id) {
                    continuation.resumeWith(Result.success(it))
                }
                runBlocking {
                    timerChannel.enqueue(stopAction)
                }
            }

            when (stopResult) {
                TimerStopResult.JOB_HAS_COMPLETED, TimerStopResult.NO_TIMER_FOUND -> {
                    error("cannot update a cart that is not in waiting state (${CartStatus.NEW.value} status)")
                }
                TimerStopResult.CANCEL_SUCCESS -> {
                    /**
                     * when duplicate cart id found and the cart is still in waiting state,
                     * after successfully stop the timer, we return all items and clear cart-item set in redis
                     * to prevent the cart from having duplicate items of a kind
                     */
                    logger.info ("cancelling current cart...")
                    returnItems(currentCart)
                    cartChannel.enqueue(CartAction.ClearCartItems(currentCart.id))
                    logger.info ("cancelling current cart completed")
                }
            }
        }

        if (cart.items.distinctBy { it.itemId }.size != cart.items.size) error ("cart has duplicate item")

        val successList = mutableListOf<CartItem>()
        val failureList = mutableListOf<CartItem>()

        val resultChannel = Channel<ReserveItemResult>()
        for(item in cart.items) {
            // a very overkill fan-out-in for this case.
            // note that if run with coroutineScope{ }, it is sequential, not concurrent
            CoroutineScope(coroutineContext).launch {
                logger.debug("enqueue item: $item, in context: $coroutineContext")
                //flow
                itemChannel.enqueue(ItemAction.Reserve(item.itemId, item.count, resultChannel))
            }
        }

        val actionTime = LocalDateTime.now(ZoneOffset.UTC)

        logger.debug("waiting for result...")
        resultChannel
            .consumeAsFlow()
            .take(cart.items.size)
            .collect { result ->
                /**
                 * TODO if something happens in the queueing, this will never completes.
                 * set timeout to cancel items that have not been processed(removing queued actions from the channel?)
                 * we can't rollback the successful operations
                 */
                logger.debug("receive from result channel: $result")
                val item = cart.items.first { it.itemId == result.itemID }
                if (result.success) {
                    successList.add(item.copy(addedAt = actionTime))
                } else {
                    failureList.add(item)
                }
            }

        resultChannel.close()

        logger.debug("result: success: $successList, failure $failureList")
        if (successList.isNotEmpty()) {
            val flattened = successList
                .map { CartItemMapper.flatten(it) }

            withJedis {
                sadd(CartItemMapper.createHashKey(cart.id),
                *flattened.toTypedArray())
            }
            withJedis { hset(
                CartMapper.createHashKey(cart.id),
                CartMapper.createValueMap(cart.copy(createdAt = actionTime))
            ) }

            val timerAction = TimerAction.Set(cart.id, CART_TIMEOUT_MILLIS) {
                CoroutineScope(Dispatchers.Default).launch {
                    setCartToExpire(cart.id)
                        .also {
                            if (it == -1)
                                logger.debug("can't timeout on ${CartStatus.COMPLETED.value} cart")
                            else
                                logger.debug("cart:${cart.id} is now timeout")
                        }
                }
            }

            timerChannel.enqueue(timerAction)
        }
        return AddCartResponse(successList, failureList)
    }

    override suspend fun checkout(id: String): Int {
        val checkoutResult = suspendCancellableCoroutine<Int> {
            val checkoutAction = CartAction.Checkout(id) { inserted ->
                /**
                 *
                 * the callback is called in channel's context/execution sequence. Make sure we don't execute too many heavy
                 * operations here because it prevents the channel to executing its next item
                 */
                logger.debug("get result from checkout action: $inserted")
                it.resumeWith(Result.success(inserted))
            }
            runBlocking {
                logger.debug("enqueuing checkout")
                cartChannel.enqueue(checkoutAction)
            }
        }

        /**
         * checkout result is 0 if success (a direct value from redis which means no new data added)
         * it is -1 if the cart has been timeout. see [internalCheckout]
         * TODO a more descriptive way of propagating/communicating result
         */
        logger.debug("checkout returns: $checkoutResult")

        if (checkoutResult == 0) {
            timerChannel.enqueue(TimerAction.Stop(id))
        }

        return checkoutResult
    }

    private fun internalCheckout(action: CartAction.Checkout) {
        val cartKey = CartMapper.createHashKey(action.cartID)
        logger.debug("internal checkout on $cartKey")
        val currentStatus = withJedis { hget(cartKey, CartMapper.STATUS) }
        if (currentStatus == CartStatus.TIMEOUT.value)
            action.cb(-1)
        else
            withJedis { hset(cartKey, CartMapper.STATUS, CartStatus.COMPLETED.value) }
                .toInt()
                .also { action.cb(it) }
    }

    override suspend fun setCartToExpire(id: String): Int {
        //a simple callback-like in coroutine, ref: https://medium.com/swlh/kotlin-coroutines-in-android-suspending-functions-8a2f980811f8
        val expireResult = suspendCancellableCoroutine<Int> {
            val checkoutAction = CartAction.Timeout(id) { updated ->
                logger.debug("get result from timeout action: $updated")
                it.resumeWith(Result.success(updated))
            }
            runBlocking {
                logger.debug("enqueuing timeout")
                cartChannel.enqueue(checkoutAction)
            }
        }.also { logger.debug("timeout returns: $it")}

        if (expireResult == -1) return expireResult

        returnItems(getCart(id)!!)

        return expireResult
    }

    private fun internalTimeout(action: CartAction.Timeout) {
        val cartKey = CartMapper.createHashKey(action.cartID)
        logger.debug("internal timeout on $cartKey")
        val currentStatus = withJedis { hget(cartKey, CartMapper.STATUS) }
        if (currentStatus == CartStatus.COMPLETED.value)
            action.cb(-1)
        else
            withJedis { hset(cartKey, CartMapper.STATUS, CartStatus.TIMEOUT.value) }
                .toInt()
                .also { action.cb(it) }
    }

    private fun reserveItem(itemAction: ItemAction.Reserve) {
        val itemKey = ItemMapper.createHashKey(itemAction.itemID)
        val requested = itemAction.count

        if(requested <= 0) error ("reserve item count cannot be 0 or negative")
        runBlocking {
            val stock = withJedis { hget(itemKey, ItemMapper.STOCK)}.toIntOrNull()?: 0
            if (stock < requested) {
                logger.debug("reserve $itemKey failed")
                itemAction.resultChannel.send(ReserveItemResult(itemAction.itemID, false))
            } else {
                val actionTime = LocalDateTime.now(ZoneOffset.UTC)
                logger.debug("reserve $itemKey success")
                withJedis { hincrBy(itemKey, ItemMapper.STOCK, requested.unaryMinus().toLong()) }.also { logger.debug("incr res: $it")}
                withJedis { hset(itemKey, ItemMapper.UPDATED_AT, Item.createString(actionTime)) }.also { logger.debug("update res: $it")}
                logger.debug("sending result to channel: ${itemAction.resultChannel} close for send${itemAction.resultChannel.isClosedForSend}")
                itemAction.resultChannel.send(ReserveItemResult(itemAction.itemID, true))
                logger.debug("result sent! ${itemAction.resultChannel}")
            }
        }
    }

    private fun returnItem(itemAction: ItemAction.Return) {
        val itemKey = ItemMapper.createHashKey(itemAction.itemID)
        val returned = itemAction.count
        if(returned <= 0) error ("return item count cannot be <= 0")
        logger.debug("internal returning: $itemKey stock: $returned")
        withJedis { hincrBy(itemKey, ItemMapper.STOCK, returned.toLong()) }
    }

    private suspend fun returnItems(cart: Cart) {
        cart.items.forEach {
            logger.debug("queuing action to return item:${it.itemId} from cart:$cart.id")
            itemChannel.enqueue(ItemAction.Return(it.itemId, it.count))
        }
    }

    private fun clearCartItems(action: CartAction.ClearCartItems) {
        val cartItemId = CartItemMapper.createHashKey(action.cartID)
        withJedis {
            del(cartItemId)
        }
    }

    fun <T>withJedis(block: Jedis.() -> T): T {
        return jedisPool.resource.use {
            it.block()
        }
    }
}