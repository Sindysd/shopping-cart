DROP TABLE IF EXISTS public.cart CASCADE;
DROP TABLE IF EXISTS public.item CASCADE;
DROP TABLE IF EXISTS public.cart_status CASCADE;
DROP TABLE IF EXISTS public.cart_item CASCADE;

CREATE TABLE public.item
(
    item_id CHARACTER VARYING(36) NOT NULL,
    item_name CHARACTER VARYING(100) NOT NULL,
    stock INTEGER NOT NULL DEFAULT 0,
    initial_stock INTEGER NOT NULL DEFAULT 0,
	created_at TIMESTAMP NOT NULL,
	updated_at TIMESTAMP,
	CONSTRAINT item_stock__positive_value_check CHECK (stock >= 0),
    CONSTRAINT item__pkey PRIMARY KEY (item_id)
)
TABLESPACE pg_default;

CREATE TABLE public.cart_status
(
	status CHARACTER VARYING(20) NOT NULL,
	CONSTRAINT cart_status__pkey PRIMARY KEY(status)
) 
TABLESPACE pg_default;

ALTER TABLE public.cart_status
	OWNER to sindy;


CREATE TABLE public.cart
(
    cart_id CHARACTER VARYING(36) NOT NULL,
	status CHARACTER VARYING(36) NOT NULL DEFAULT 'new', 
	created_at TIMESTAMP NOT NULL,
	CONSTRAINT cart__pKey PRIMARY KEY (cart_id),
	CONSTRAINT cart__cart_status__fKey FOREIGN KEY (status)
		REFERENCES cart_status(status)
)
TABLESPACE pg_default;

CREATE TABLE public.cart_item
(
	cart_id CHARACTER VARYING(36) NOT NULL,
	item_id CHARACTER VARYING(36) NOT NULL,
    item_count INTEGER NOT NULL,
	added_at TIMESTAMP NOT NULL,
	CONSTRAINT cart_item__item_count__min_one_check CHECK (item_count > 0),
	CONSTRAINT cart_item__item__fKey FOREIGN KEY (item_id)
		REFERENCES item(item_id),
	CONSTRAINT cart_item__cart__fKey FOREIGN KEY (item_id)
		REFERENCES item(item_id),
	CONSTRAINT cart_item__pKey PRIMARY KEY (cart_id, item_id)
)
TABLESPACE pg_default;

ALTER TABLE public.item
    OWNER to sindy;

ALTER TABLE public.cart
	OWNER to sindy;
	
ALTER TABLE public.cart_item
	OWNER to sindy;
	
INSERT INTO public.cart_status VALUES ('new'), ('completed'), ('timeout');

INSERT INTO public.item VALUES
	('1','item 1', 10,10, clock_timestamp()),
	('2','item 2', 20,20, clock_timestamp()),
	('3','item 3', 30,30, clock_timestamp());
